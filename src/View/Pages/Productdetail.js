import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { axiosGet, axiosPost } from "../../lib/withPost";
import { Container, Form, Row, Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import ImgCoco from "../../asset/images/coconut-frame.png";
import ImgLogo from "../../asset/images/logo_cycle.svg";
import "../../index.css";
const Productdetail = () => {
  const [validated, setValidated] = useState(false);
  const styles = (theme) => ({
    content1: {
      overflowY: "scroll",
    },
    btngreenlink: {
      margin: "1rem",
      textAlign: "center",
      textDecoration: "none",
      display: "block",
      lineHeight: "2.5rem",
      margin: "auto",
      display: "block",
      width: "100%",
      fontSize: "1rem",
      color: "#fcf3d1",
      background:
        "linear-gradient(#1b4e4a, #1b4e4a) padding-box, /*this is your grey background*/ linear-gradient(to bottom, #fcf3d1, #beb07b) border-box",
      border: "0.15rem solid transparent",
      borderRadius: "0.2rem",
      webkitBorderRadius: "0.2rem",
    },
    footer: {},
  });
  const handleSubmit = (event) => {};
  return (
    <div id="root-white">
      <div className="menu-header-gallery">
        <img src={ImgCoco} alt="" />
      </div>
      <div className="content-gallery">
        <div className="content-gallery-text1">
          <div className="content-gallery-text1-title">มะพร้าวลูกเกรดพรีเมี่ยมคัดพิเศษจากฟาร์ม AAA</div>
          <div className="content-gallery-text1-price">1,800 บาท/เดือน</div>
        </div>
        <div className="content-gallery-text2">
        <img src={ImgLogo} alt="logo" />
        <span style={{lineHeight: "40px",marginLeft: "10px"}}>POTO1 COCONUT</span>
        </div>
        <div className="content-gallery-text3">
          <div className="content-gallery-text3-title">รายละเอียดสินค้า</div>
          <div className="content-gallery-text3-detail">
            มะพร้าวลูกเกรดพรีเมี่ยมคัดพิเศษจกฟาร์ม AAA 🌴 จัดส่งให้ 15
            ลูก/อาทิตย์ 🌴 60 ลูก/เดือน ไม่ได้ปลอกเปลือกนะค่ะ
            เนื่องจากถ้าปลอกเปลือกแล้วขนส่งจะทำให้สินค้าเสียหายได้
            รับประกันคุณภาพสินค้า และคุณภาพการจัดส่ง
            หากสินค้าเสียหายหรือไม่ได้คุณภาพส่งสินค้าใหม่ให้ทันที 🌴
            รับประกันว่า น้ำหวาน หอม อร่อย เนื้อกำลังกิน 🌴 ส่งตรงจากสวน
          </div>
        </div>
      </div>
      <div>
        <div className="menu-footer" style={styles.footer}>
          <Link
            to="/register"
            className="btn-green"
            style={{
              margin: "1rem",
              textAlign: "center",
              textDecoration: "none",
              display: "block",
              lineHeight: "2.5rem",
            }}
          >
            สมัครรับ
            {/* <button className="btn-green" type="button"  name="nextstep" style={{ margin:"1rem"}}  >
          สมัครรับ 
          </button> */}
          </Link>
        </div>
      </div>
    </div>
  );
};
export default Productdetail;
