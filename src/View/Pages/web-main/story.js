
	import React from "react";

	import iphone from "../../../asset/imagesweb/iphone.png";
	import Logo_AAA from "../../../asset/imagesweb/Logo_AAA.svg";
	import deep_background_img from "../../../asset/imagesweb/deep_background_img.jpeg";
	import strand_manchine from "../../../asset/imagesweb/strand_manchine.svg";
	import twin_coconuts from "../../../asset/imagesweb/twin_coconuts.svg";
	import Mask_Group from '../../../asset/imagesweb/Mask_Group.png';
	export default function MyComponent1() {
		
	  return (<div className="container-p2">
  <div className="grid-container-p2">
    <div className="head_menu-p2">
      <img className="top_bg_img-p2" src={deep_background_img} />
      <div className="filter-p2" />
      <img className="logo-p2" src={Logo_AAA} />
    </div>
    <div className="content-p2">
      <img className="main_coconut-p2" src={Mask_Group} />
      <div className="leftarea-p2">
        <p className="main-p2 darkgreen-p2 font48-p2 text1-p2">เกี่ยวกับเรา</p>
        <p className="describe-p2 font36-p2 grey-p2 text2-p2">ในฐานะเกษตรกรมะพร้าว
          ผมขอใช้พื้นที่นี้ในการอธิบายให้ทุกท่านได้รู้จักและเข้าใจมะพร้าวน้ำหอมลูกเขียวเกรดพรีเมียม
          ของสวนเราให้มากขึ้นครับ เอกลักษณ์ที่สำคัญคือ มะพร้าวของเราจะต้องเป็น “มะพร้าวลูกเขียว” เท่านั้น</p>
      </div>
      <div className="rightarea-p2">
        <p className="main-p2 font48-p2 darkgreen-p2 text3-p2">มะพร้าวลูกเขียว และมะพร้าวควั่น ต่างกันอย่างไร?</p>
        <p className="describe-p2 font36-p2 grey-p2 text4-p2">
          โดยปกติมะพร้าวที่เห็นตามท้องตลาดส่วนใหญ่มักจะถูกควั่นหรือปอกเปลือกเขียวออกและต้องนำไปแช่ในสารละลายฟอกขาวเพื่อยับยั้งการเกิดเชื้อราซึ่งสารละลายนี้อาจซึมลงไปในตามะพร้าวจนเกิดเป็นสารเคมีตกค้างไปถึงผู้บริโภคได้
          ด้วยเหตุนี้ เราจึงเลือกที่จะรักษาความเป็น “มะพร้าวลูกเขียว” เอาไว้ เพื่อขจัดการใช้สารเคมีต่างๆ
          และรักษากระบวนการบ่มมะพร้าวให้เป็น ไปตามธรรมชาติ เพื่อให้ได้น้ำมะพร้าวที่มีรสชาติหวาน หอม อร่อยแบบ
          ดั้งเดิมตามธรรมชาติและยังคงคุณค่าสารอาหารที่มีดีมีประโยชน์ต่อสุขภาพอย่างเต็มที่</p>
      </div>
      <img className="second_coconut-p2" src={twin_coconuts} />
      <div className="leftarea-p2">
        <p className="main-p2 darkgreen-p2 font48-p2 text5-p2">สร้างนวัตกรรมใหม่ เพื่อชีวิตที่ง่ายยิ่งขึ้น</p>
        <p className="describe-p2 font36-p2 grey-p2 text6-p2">ในการคัดคุณภาพมะพร้าวเกรดพรีเมียม สวนของเราใช้นวัตกรรม เครื่อง NIR (Near Infrared Spectrometer) 
          ที่ได้มาตรฐานสากล เพื่อคัดสรร มะพร้าวคุณภาพที่ดีที่สุดสำหรับผู้บริโภค นอกจากนี้เรายังได้คิดค้น 
          นวัตกรรมที่อำนวยความสะดวกแก่ลูกค้าในการบริโภคมะพร้าวลูกเขียว คือ ด้วย “เครื่องเจาะมะพร้าว” 
          ที่ช่วยทุ่นแรงลูกค้าให้สามารถเจาะลูกมะพร้าว ได้ง่าย สะดวก และรวดเร็วยิ่งขึ้น</p>
      </div>
      <img className="machine_image-p2" src={strand_manchine} />
      <div className="rightarea-p2">
        <p className="main-p2 font48-p2 grey-p2 text7-p2">สร้างช่องทางการขายใหม่ จูงใจเกษตรกร</p>
        <p className="describe-p2 font36-p2 grey-p2 text8-p2">นอกจากความต้องการที่อยากให้ลูกค้าได้บริโภคมะพร้าวที่มีประโยชน์ดี
          ต่อสุขภาพแล้ว เรายังต้องการสร้างช่องทางการขายใหม่สำหรับการ 
          จำหน่ายสินค้าทางการเกษตรที่มีคุณภาพ ลดการพึ่งพาพ่อค้าคนกลาง 
          และเพิ่มรายได้ให้มากกว่าการขายหน้าสวน ทั้งนี้ เพื่อเป็นแนวทางและ 
          แรงจูงใจที่ช่วยจูงใจให้เกษตรกรชาวสวนราย ท่านอื่นๆ ได้ผลิตสินค้าที่มี 
          คุณภาพที่ดีมากยิ่งขึ้น ตอบสนองความต้องการของลูกค้าที่ต้องการ 
          สินค้าที่มีคุณภาพสูงโดยเกษตรกรสามารถเลือกนำสินค้ามาจำหน่ายใน 
          ช่องทางการขายใหม่ที่เราตั้งใจสร้างขึ้น หรือในช่องทางอื่นๆ เพิ่มเติมก็ได้</p>
      </div>
      <img className="mobile_image-p2" src={iphone} />
    </div>
  </div>
  <div className="footer-p2">
    <p className="footer_text-p2 white-p2">© 2021 POTO1 PREMIUM FRUIT. ALL RIGHTS RESERVED.</p>
  </div>
</div>

	)};
