import React from "react";
import "../../../cssweb/main-style.css";
import ham from "../../../asset/imagesweb/hamburger_icon.svg";
import f2177 from "../../../asset/imagesweb/Frame_2177.png";
import fourth_img from "../../../asset/imagesweb/fourth_img.png";
import Frame_2176 from "../../../asset/imagesweb/Frame_2176.png";
import image_24 from "../../../asset/imagesweb/image_24.png";
import Frame_2179 from "../../../asset/imagesweb/Frame_2179.png";
import Logo_AAA from "../../../asset/imagesweb/Logo_AAA.svg";
import deep_background_img from "../../../asset/imagesweb/deep_background_img.jpeg";
import Mask_Group from "../../../asset/imagesweb/Mask_Group.png";
import { Link } from "react-router-dom";

export default function MyComponent() {
  return (
    <div className="container-p1">
      <div className="grid-container-p1">
        <div className="first-p1" id="first-p1">
          <div className="dropdown-p1">
            <img
              onClick={function (e) {
                document.getElementById("myDropdown").classList.toggle("show");
              }}
              className="hamburger_icon-p1"
              src={ham}
            />
            <div id="myDropdown" className="dropdown-content-p1">
              <a className="sub-describe-p1 darkgreen-p1" href="#second">
                ผลิตภัณฑ์ของเรา
              </a>
              <a className="sub-describe-p1 darkgreen-p1" href="#third">
                ระบบสมาชิกรายเดือน
              </a>
              <a className="sub-describe-p1 darkgreen-p1" href="#fourth">
                วิธีการสั่งซื้อ
              </a>
              <a className="sub-describe-p1 darkgreen-p1" href="#fifth">
                เกี่ยวกับเรา
              </a>
              <a
                className="sub-describe-p1 darkgreen-p1"
                target="_self"
                href="/cancelation-policy"
              >
                นโยบายคืนเงิน
              </a>
            </div>
          </div>
          <img className="top_bg_img-p1" src={deep_background_img} />
          <div className="filter-first-p1" />
          <img className="logo-p1" src={Logo_AAA} />
          <ul className="top_menu-p1">
            <li className="nolist_icon-p1">
              <a className="white-p1" href="#second">
                ผลิตภัณฑ์ของเรา
              </a>
            </li>
            <li className="nolist_icon-p1">
              <a className="white-p1" href="#third">
                ระบบสมาชิกรายเดือน
              </a>
            </li>
            <li className="nolist_icon-p1">
              <a className="white-p1" href="#fourth">
                วิธีการสั่งซื้อ
              </a>
            </li>
            <li className="nolist_icon-p1">
              <a className="white-p1" href="#fifth">
                เกี่ยวกับเรา
              </a>
            </li>
            <li className="nolist_icon-p1">
              <a className="white-p1" target="_self" href="/cancelation-policy">
                นโยบายคืนเงิน
              </a>
            </li>
          </ul>
          <div className="leftarea-p1">
            <p className="title-p1 gold-p1 bold-p1 text1-p1">
              POTO1 Premium Fruits
            </p>
            <p className="main-p1 white-p1 font48-p1 bold-p1 text2-p1">
              มะพร้าวน้ำหอม จากสวนผู้ผลิต คุณภาพระดับพรีเมี่ยม
            </p>
            <p className="describe-p1 white-p1 font36-p1 text3-p1">
              จำหน่ายมะพร้าวลูก และน้ำมะพร้าว ระบบการจัดส่งรวดเร็ว ปลอดภัย
            </p>
          </div>
          <div>
            <img className="main_coconut-p1" src={f2177} />
          </div>
        </div>
        <div className="second-p1" id="second">
          <div className="rightarea-p1">
            <p className="describe-p1 font36-p1 grey-p1 text4-p1">
              ผลิตภัณฑ์ของเรา
            </p>
            <p className="main-p1 font48-p1 darkgreen-p1 text5-p1">
              เรามีมะพร้าวน้ำหอมลูกเขียว ก้นจีบ เกรดพรีเมี่ยม คัดพิเศษ สดใหม่
              ส่งตรงจากสวนของเราที่ อ.ดำเนินสะดวก จ.ราชบุรี
            </p>
            <p className="sub-describe-p1 darkgrey-p1 text6-p1">
              พร้อมจำหน่ายทั้งแบบปลีก และแบบเหมารายเดือน
            </p>
          </div>
          <div>
            <img className="second_coconut-p1" src={Frame_2179} />
          </div>
        </div>
        <div className="third-p1" id="third">
          <div>
            <img className="third_coconut-p1" src={image_24} />
          </div>
          <div className="leftarea-p1">
            <p className="describe-p1 font36-p1 grey-p1 text7-p1">
              ระบบสมาชิกรายเดือน
            </p>
            <p className="main font48-p1 darkgreen-p1 text8-p1">
              มั่นใจได้ว่าคุณจะมีมะพร้าวสดใหม่ จัดส่งให้คุณทุกอาทิตย์ ตลอดเดือน
            </p>
            <p className="sub-describe-p1 darkgrey-p1 text9-p1">
              เราบริการขายสินค้ารูปแบบสมาชิกรายเดือนซึ่งพร้อมจัดส่งสินค้าที่ท่าน
              เลือกสมัครรับเป็นประจำทุกสัปดาห์ ตลอดเดือน
              โดยท่านสามารถเลือกยกเลิกการเป็นสมาชิกได้ทุกเมื่อ
            </p>
          </div>
          <div className="leftarea-p1">
            <div className="btn_position1-p1">
              {/* <form action="/productlist" method="get"> */}
              <Link to="/productlist">
                {" "}
                <button
                  className="mybutton-p1 button_text-p1 btn-darkgreen-p1"
                  type="submit"
                  id="scribe"
                >
                  สมัครรับ
                </button>
              </Link>
              {/* </form> */}
            </div>
          </div>
        </div>
        <div className="fourth-p1" id="fourth">
          <div>
            <img className="fourth_image-p1" src={Frame_2176} />
            <img className="fourth_image2-p1" src={fourth_img} />
          </div>
          <div className="rightarea-p1">
            <p className="describe-p1 font36-p1 grey-p1 text10-p1">
              วิธีการสั่งซื้อ
            </p>
            <p className="main-p1 font48-p1 darkgreen-p1 text11-p1">
              สั่งซื้อสินค้าผ่านร้านของเราบน
            </p>
            <p className="main-p1 font48-p1 lightgreen-p1 text12-p1">
              {" "}
              LINE Official
            </p>
            <p className="sub-describe-p1 darkgrey-p1 text13-p1">
              1. เป็นเพื่อนกับเรา LINE
              <br />
              2. สมัครสมาชิก พร้อมกรอกที่อยู่ในการจัดส่ง
              <br />
              3. เลือกผลิตภัณฑ์ที่ต้องการสมัครรับ
              <br />
              4. ชำระเงินผ่านบัตรเครดิต
              <br />
              5. รอรับสินค้าสดใหม่ที่จะจัดส่งให้ทุกอาทิตย์ถึงบ้าน
            </p>
          </div>
          <div className="rightarea-p1">
            <div className="btn_position2-p1">
              {/* <form action="/productlist" method="get"> */}
              <Link to="/productlist">
                <button
                  className="mybutton-p1 button_text-p1 btn-darkgreen-p1"
                  type="submit"
                  id="scribe"
                >
                  สมัครรับ
                </button>
              </Link>
              {/* </form> */}
            </div>
          </div>
        </div>
        <div className="fifth-p1" id="fifth">
          <div>
            <img className="fifth_coconut-p1" src={Mask_Group} />
          </div>
          <img className="fifth_bg_img-p1" src={deep_background_img} />
          <div className="filter-fifth-p1" />
          <div className="leftarea-p1">
            <p className="describe-p1 font36-p1 white-p1 text14-p1">
              เกี่ยวกับเรา
            </p>
            <p className="sub-describe-p1 white-p1 text15-p1">
              ในฐานะเกษตรกรมะพร้าว
            </p>
            <p className="sub-describe-p1 white-p1 text16-p1">
              ผมขอใช้พื้นที่นี้ในการอธิบายให้ทุกท่านได้รู้จักและเข้าใจมะพร้าวน้ำหอมลูกเขียวเกรดพรีเมียม
              ของสวนเราให้มากขึ้นครับ เอกลักษณ์ที่สำคัญคือ
              มะพร้าวของเราจะต้องเป็น “มะพร้าวลูกเขียว” เท่านั้น
            </p>
          </div>
          <ul className="sub-describe-p1 white-p1 text17-p1 list_icon-p1">
            <li>
              <a href="/story" className="white-p1">
                มะพร้าวลูกเขียว และมะพร้าวควั่นต่างกันอย่างไร?
              </a>
            </li>
            <li>
              <a href="/story" className="white-p1">
                สร้างนวัตกรรมใหม่ เพื่อชีวิตที่ง่ายยิ่งขึ้น
              </a>
            </li>
            <li>
              <a href="/story" className="white-p1">
                สร้างช่องทางการขายใหม่ จูงใจเกษตรกร
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div className="footer-p1">
        <p className="footer_text-p1 white-p1">
          © 2021 POTO1 PREMIUM FRUIT. ALL RIGHTS RESERVED.
        </p>
      </div>
    </div>
  );
}
