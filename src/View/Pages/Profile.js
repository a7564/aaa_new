import React from 'react'
import { Link } from 'react-router-dom'
import Menufooter from '../../Layout/menufooter'
import Menuheader from '../../Layout/menuheader'
const Profile = () => {
  return (
    <>
      <div id="root-green">
        <Menuheader name="โปรไฟล์" icon="profile" />
        <div className="content">
          <div className="profileBlock">
            <span class="profileImg"></span>
            <div class="ProfileName">
              <b>สมจิตร นวลจันทร์</b>
            </div>
            <div class="ProfileTel">089 123 4567</div>
            <div class="ProfileEmail">somjit.nuan@gmail.com</div>
          </div>

          <div class="AddrList">
            <div class="AddrHead">ที่อยู่ในการจัดส่งผลิตภัณฑ์</div>
            <div class="AddrBlock">
              <div class="AddrBlockDetail">
                <div>
                  <div class="AddrName">สมจิตร นวลจันทร์</div>
                  <div class="AddrTel">089 123 4567</div>
                  <div class="AddrDetail">
                    333 ซอยเฉยพ่วง ถนนวิภาวดีรังสิต แขวงจอมพล เขตจตุจักร
                    กรุงเทพมหานคร 10900
                  </div>
                </div>
              </div>
              <div class="btnAddrEdit">
                <span></span>
              </div>
            </div>

            <div class="AddrBlock">
              <div class="AddrBlockDetail">
                <div>
                  <div class="AddrName">สมจิตร นวลจันทร์</div>
                  <div class="AddrTel">089 123 4567</div>
                  <div class="AddrDetail">
                    333 ซอยเฉยพ่วง ถนนวิภาวดีรังสิต แขวงจอมพล เขตจตุจักร
                    กรุงเทพมหานคร 10900
                  </div>
                </div>
              </div>
              <div class="btnAddrEdit">
                <span></span>
              </div>
            </div>
          </div>

          <div class="AddrNew">
              <a href="#">เพิ่มที่อยู่ใหม่</a>
            </div>
        </div>
        <Menufooter></Menufooter>
      </div>
    </>
  )
}
export default Profile
