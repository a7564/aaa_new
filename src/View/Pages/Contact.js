import React from "react";
import Image from "../../asset/images/coconut.svg";
import Menufooter from "../../Layout/menufooter";
import Menuheader from "../../Layout/menuheader";

const Contact = () => {
  return (
    <div id="root-green">
      <Menuheader name="ติดต่อเรา" icon="contact" />
      <div
        className="content"
        style={{ color: "#FFF", fontSize: "1.25rem", fontWeight: "normal" }}
      >
        <center>
          <img src={Image} alt={"Coco"} />
          <br></br>
          ต้องการความช่วยเหลือด้านการใช้งาน
          <h2>ต้องการยกเลิกสมาชิก</h2>
          ติดต่อศูนย์บริการ AAA Premium
          <br></br>
          โทร
          <br></br>
          <h1>02 123 4567</h1>
          <div style={{ padding: "0 1rem 0 1rem" }}>
            <div>
              <button
                className="btn-green"
                type="button"
                name="tel"
                onClick={(e) => {
                  e.preventDefault();
                  window.location.href = "tel:+021234567";
                }}
                style={{    padding: "1rem" }}
              >
                โทร
              </button>
            </div>
          </div>
        </center>
      </div>
      <Menufooter hightlight="contactG" />
    </div>
  );
};
export default Contact;
