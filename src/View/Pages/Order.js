import React from "react";
import Menufooter from "../../Layout/menufooter";
import Menuheader from "../../Layout/menuheader";
import {  Link } from "react-router-dom";
const Profile = () => {
  return (
    <div id="root-green">
      <Menuheader name="การรับสมัคร" icon="order" />
      <div class="content">

          <h1>การรับสมัคร</h1>
          <Link to='/register' > Click Here.. </Link>
      </div>
      <Menufooter  hightlight="orderG" />
    </div>
  );
};
export default Profile;
