import React, { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Container, Form, Row, Col, Button } from 'react-bootstrap'
import Logo from '../../asset/images/logo-big.svg'
import { Link } from 'react-router-dom'
import { axiosPost } from '../../lib/withPost'
import history from '../../Components/history'
import dataheaderJson from '../../Components/dataheaderAxios'
import Errordialog from '../../Components/errordialog'
const Login = () => {
  const [validated, setValidated] = useState(false)
  const [open, setDisplayerror] = useState('none')
  const [displayerrorTitle, setDisplayerrorTitle] = useState('')
  const [displayerrorMessage, setDisplayerrorMessage] = useState('')
  const [input, setInput] = useState({
    username: '',
    password: '',
  })
  const handleSubmit = async (event) => {
    const form = event.currentTarget
    alert(form.checkValidity())
    let isValid = false
    if (form.checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    } else {
      if (input.password !== '' && input.username !== '') {
        isValid = true
      }

      if (isValid === true) {
        const dt = {
          authenToken:
            'ewogICJzeXN0ZW0iOiJBQUFQcmVtaXVtIiwKICAidXNlciI6Im1pY3Jvc2l0ZS1zZXJ2aWNlIgp9',
          authenUser: 'microsite',
          sessionID:
            'a7e4bb69ba740ceabdd009bade42d36b06176098d144b3513efa79f5d075a8e2',
          language: navigator.language,
          ipAddress: window.ipAddress,
          userAgent: navigator.userAgent,
        }
        let data = { ...dt, ...input }
        console.log('data', data)
        const resp = await axiosPost('/authen/login-username.service', data)
        console.log('data', resp)
        if (!resp.data) {
          if (resp.res.responseCode !== '200') {
            setDisplayerrorTitle(resp.res.responseCode)
            setDisplayerrorMessage(resp.res.responseMessage)
            setDisplayerror('none')
            setDisplayerror('block')
            return
          }
        } else {
          if (resp.data.responseCode === '200') {
            history.push('/productlist')
            return
          } else {
            setDisplayerrorTitle(resp.data.responseCode)
            setDisplayerrorMessage(resp.data.responseMessage)
            setDisplayerror('none')
            setDisplayerror('block')
            return
          }
          // console.log("DataH : ", resp.data);
          // console.log("DataH1 : ", resp.res.responseCode);
        }
      }
    }
    setValidated(true)
  }
  const handleChange = (e) => {
    const { target } = e
    const { name } = target
    const value = target.value
    setInput({
      ...input,
      [name]: value,
    })
  }
  return (
    <div>
      <div id="root-green">
        <Errordialog
          open={open}
          title={displayerrorTitle}
          message={displayerrorMessage}
        />
        <div style={{ padding: '0 5%', color: '#FCF3D1' }}>
          <Container>
            <Row>
              <Col className="loginPanel">
                <center><img src={Logo} alt="LogoBig" style={{ width: '32%' }} /></center>
                <Form validated={validated}>
                  <Row className="mb-3">
                    <Form.Group className="mb-3">
                      <Form.Label>อีเมล์</Form.Label>
                      <Form.Control
                        required
                        type="email"
                        placeholder="example@email.com"
                        name="username"
                        id="username"
                        maxLength="50"
                        onChange={handleChange}
                        style={{ opacity: '0.5' }}
                      />
                      <Form.Text className="text-muted"></Form.Text>
                    </Form.Group>
                    <Form.Group className="mb-3">
                      <Form.Label>รหัสผ่าน</Form.Label>
                      <Form.Control
                        required
                        type="password"
                        name="password"
                        maxLength="20"
                        onChange={handleChange}
                        style={{ opacity: '0.5' }}
                        placeholder="********"
                      />
                      <Form.Text className="text-danger"></Form.Text>
                    </Form.Group>
                  </Row>
                </Form>
                <Form.Group
                  className="mb-3"
                  style={{ width: 'auto', marginTop: '-25px' }}
                >
                  <Form.Label style={{ float: 'right','margin-bottom': '20px' }}>
                    <Link to="" style={{ color: '#FCF3D1' }}>
                      ลืมรหัสผ่าน?
                    </Link>
                  </Form.Label>
                </Form.Group>
                <Form.Group className="mb-3">
                  <Button
                    className="btn-green"
                    type="button"
                    name="login"
                    onClick={handleSubmit}
                  >
                    เข้าสู่ระบบ
                  </Button>
                </Form.Group>
                <hr
                  style={{
                    width: '90%',
                    margin: '2rem auto',
                    opacity: '1',
                    color: '#FCF3D1',
                  }}
                ></hr>
                <center>
                  <Form.Group className="mb-3" style={{ width: 'auto' }}>
                    <Form.Label>
                      <Link to="/register" style={{ color: '#FCF3D1' }}>
                        สมัครสมาชิกใหม่
                      </Link>
                    </Form.Label>
                  </Form.Group>
                </center>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
    </div>
  )
}
export default Login
