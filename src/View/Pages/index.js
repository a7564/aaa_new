import * as React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../index.css"
import ImgCoco from "../../asset/images/coconut.png";
import Menufooter from "../../Layout/menufooter";
import Menuheader from "../../Layout/menuheader";
import {  Link } from "react-router-dom";
const Home = () => {
  return (
    <div id="root-green">
      <Menuheader name="ผลิตภัณฑ์" icon="home" />
      <div className="content">
        <div className="c-title">ผลิตภัณฑ์ทั้งหมด</div>
        <Link to="/productdetail" alt="สินค้าคุณภาพ">
        <div className="c-card">
          <img src={ImgCoco} alt="" />
          <div className="c-detail">
            <div className="c-detail-1">
              มะพร้าวลูกเกรดพรีเมี่ยมคัดพิเศษจากฟาร์ม AAA
            </div>
            <div className="c-detail-2">POTO1 COCONUT</div>
            <div className="c-detail-3">1,800 บาท/เดือน</div>
            <div className="c-sales">
              สินค้าขายดี
              <div />
            </div>
          </div>
        </div>
        </Link>
        <Link to="/productdetail" alt="สินค้าคุณภาพ">
        <div className="c-card">
          <img src={ImgCoco} alt="" />
          <div className="c-detail">
            <div className="c-detail-1">
              มะพร้าวลูกเกรดพรีเมี่ยมคัดพิเศษจากฟาร์ม AAA
            </div>
            <div className="c-detail-2">POTO1 COCONUT</div>
            <div className="c-detail-3">1,800 บาท/เดือน</div>
            <div className="c-sales">
              สินค้าขายดี
              <div />
            </div>
          </div>
        </div>
        </Link>
        <Link to="/productdetail" alt="สินค้าคุณภาพ">
        <div className="c-card">
          <img src={ImgCoco} alt="" />
          <div className="c-detail">
            <div className="c-detail-1">
              มะพร้าวลูกเกรดพรีเมี่ยมคัดพิเศษจากฟาร์ม AAA
            </div>
            <div className="c-detail-2">POTO1 COCONUT</div>
            <div className="c-detail-3">1,800 บาท/เดือน</div>
            <div className="c-sales">
              สินค้าขายดี
              <div />
            </div>
          </div>
        </div>
        </Link>
        <Link to="/productdetail" alt="สินค้าคุณภาพ">
        <div className="c-card">
          <img src={ImgCoco} alt="" />
          <div className="c-detail">
            <div className="c-detail-1">
              มะพร้าวลูกเกรดพรีเมี่ยมคัดพิเศษจากฟาร์ม AAA
            </div>
            <div className="c-detail-2">POTO1 COCONUT</div>
            <div className="c-detail-3">1,800 บาท/เดือน</div>
            <div className="c-sales">
              สินค้าขายดี
              <div />
            </div>
          </div>
        </div>
        </Link>
      
      </div>
      <Menufooter hightlight="homeG"/>
    </div>
  );
};
export default Home;
