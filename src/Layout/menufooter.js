import * as React from "react";
import { Link } from "react-router-dom";
import Icon from "../Components/Icon";

class Menufooter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      iconhome: "",
      iconorder: "",
      iconprofile: "",
      iconcontact: "",
      mtext1: "",
      mtext2: "",
      mtext3: "",
      mtext4: "",
    };
  }
  componentDidMount() {
    let iconhome = "";
    let iconorder = "";
    let iconprofile = "";
    let iconcontact = "";
    let mtext_1 = "";
    let mtext_2 = "";
    let mtext_3 = "";
    let mtext_4 = "";
    this.setState(function (props) {
      if (this.props.hightlight === "homeG") {
        iconhome = Icon["HomeG"];
        iconorder = Icon["Order"];
        iconprofile = Icon["Profile"];
        iconcontact = Icon["Contact"];
        mtext_1 = "gradient";
      } else if (this.props.hightlight === "orderG") {
        iconhome = Icon["Home"];
        iconorder = Icon["OrderG"];
        iconprofile = Icon["Profile"];
        iconcontact = Icon["Contact"];
        mtext_2 = "gradient";
      } else if (this.props.hightlight === "profileG") {
        iconhome = Icon["Home"];
        iconorder = Icon["Order"];
        iconprofile = Icon["ProfileG"];
        iconcontact = Icon["Contact"];
        mtext_3 = "gradient";
      } else {
        iconhome = Icon["Home"];
        iconorder = Icon["Order"];
        iconprofile = Icon["Profile"];
        iconcontact = Icon["ContactG"];
        mtext_4 = "gradient";
      }

      return {
        iconhome: iconhome,
        iconorder: iconorder,
        iconprofile: iconprofile,
        iconcontact: iconcontact,
        mtext1: "m-text "+mtext_1,
        mtext2: "m-text "+mtext_2,
        mtext3: "m-text "+mtext_3,
        mtext4: "m-text "+mtext_4,
      };
    });
  }

  render() {
    return (
      <div>
        <div className="menu-footer">
          <Link to="/productlist" className="m-block">
            <div className="m-icon">{this.state.iconhome} </div>
            <div className={this.state.mtext1}>ผลิตภัณฑ์</div>
          </Link>
          <Link to="/order" className="m-block">
            <div className="m-icon">{this.state.iconorder} </div>
            <div className={this.state.mtext2}>รายการสั่งซื้อ</div>
          </Link>
          <Link to="/profile" className="m-block">
            <div className="m-icon">{this.state.iconprofile} </div>
            <div className={this.state.mtext3}>โปรไฟล์</div>
          </Link>
          <Link to="/contact" className="m-block">
            <div className="m-icon">{this.state.iconcontact}</div>
            <div className={this.state.mtext4}>ติดต่อเรา</div>
          </Link>
        </div>
      </div>
    );
  }
}
export default Menufooter;
