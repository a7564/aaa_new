import * as React from "react";
import Img from "../asset/logo/logo.svg";
import Imghome from "../asset/images/home.svg";
import Imgorder from "../asset/images/orderlist.svg";
import Imgprofile from "../asset/images/profile.svg";
import Imgcontact from "../asset/images/contact.svg";

class Menuheader extends React.Component {
  render() {
    const { name } = this.props;
    let icon = Imghome;
    if (this.props.icon === "home") {
      icon = Imghome;
    } else if (this.props.icon === "order") {
      icon = Imgorder;
    } else if (this.props.icon === "profile") {
      icon = Imgprofile;
    } else {
      icon = Imgcontact;
    }
    return (
      <div className="menu-header">
        <div className="header-logo-right">
          <img src={Img} alt={"Logo"} />
        </div>
        <div className="header-title-left">
          <div className="header-title-left-detail">
            <img src={icon} alt={"Home"} />
            {name}
          </div>
        </div>
      </div>
    );
  }
}
export default Menuheader;
