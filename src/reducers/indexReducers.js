import { combineReducers } from "redux";
import { itReducer } from "./itReducers";
import { userReducer } from "./userReducer";

const RootReducers = combineReducers ({
    user : userReducer,
    it : itReducer
})
export default RootReducers;
