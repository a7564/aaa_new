import React from "react";
import { Router, Route } from "react-router-dom";
import P404 from "./View/Pages/P404";
import contact from "./View/Pages/Contact";
import orderlist from "./View/Pages/Order";
import profile from "./View/Pages/Profile";
import PagesRegister from "./View/Pages/Register";
import indexweb from "./View/Pages/web-main/indexweb";
import productlist from "./View/Pages/index";
import story from "./View/Pages/web-main/story";
import cancelation_policy from "./View/Pages/web-main/cancelation_policy";
import registerconfirmotp from "./Components/registerConfirmotp";
import registersuccess from "./Components/registersuccess";
import productdetail from "./View/Pages/Productdetail";
import errordialog from "./Components/errordialog";
import login from "./View/Pages/Login";
import history from '../src/Components/history';

const App = () => {
  return (
    <div>
      <Router history={history}>
        <Route exact path="/" component={indexweb} />
        <Route path="/story" component={story} />
        <Route path="/cancelation-policy" component={cancelation_policy} />
        <Route path="/productlist" component={productlist} />
        <Route path="/order" component={orderlist} />
        <Route path="/profile" component={profile} />
        <Route path="/contact" component={contact} />
        <Route path="/register" component={PagesRegister} />
        <Route path="/login" component={login} />
        <Route path="/register-confirm-otp" component={registerconfirmotp} />
        <Route path="/registersuccess" component={registersuccess} />
        <Route path="/productdetail" component={productdetail} />
        <Route path="/errordialog" component={errordialog} />
        {/* <Route path="*" component={P404} /> */}
      </Router>
    </div>
  );
};

export default App;
