import React, { useState, useContext, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../index.css";
import Errordialog from "./errordialog";
import { axiosPost } from "../lib/withPost";
import { Container, Form, Row, Col } from "react-bootstrap";
import dataheaderJson from "../Components/dataheaderAxios";
import history from "./history";
import { CounterContext } from "../store/CounterProvider";

const ConfirmOTP = (props) => {
  const { contextrefOTP } = useContext(CounterContext);
  const [open, setDisplayerror] = useState("none");
  const [validated, setValidated] = useState(false);
  const [displayerrorTitle, setDisplayerrorTitle] = useState("");
  const [displayerrorMessage, setDisplayerrorMessage] = useState("");
  const [errorMessage, setErrorM] = useState("");

  const [inputotp, setInput] = useState({
    registerId: contextrefOTP.registerId,
    otpRef: contextrefOTP.otpRef,
    otp: "",
  });
  useEffect(() => {
    setDisplayerror("none");
  }, [props.open]);

  const handleSubmit = async (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    setValidated(true);

    let data = { ...dataheaderJson, ...inputotp };
    const resp = await axiosPost("/register/register-confirm.service", data);

    if (!resp.data) {
      if (resp.res.responseCode !== "200") {
        setDisplayerrorTitle(resp.res.responseCode);
        setDisplayerrorMessage(resp.res.responseMessage);
        setDisplayerror("none");
        setDisplayerror("block");
        return;
      }
    
    }else{
      if (resp.data.responseCode === "200") {
        history.push("/registersuccess");
        return;
      }else{
        setDisplayerrorTitle(resp.data.responseCode);
        setDisplayerrorMessage(resp.data.responseMessage);
        setDisplayerror("none");
        setDisplayerror("block");
        return;
      }
      // console.log("DataH : ", resp.data);
      // console.log("DataH1 : ", resp.res.responseCode);
    }
    
  };

  const handleSendOTPAgain = async (e) => {
  
    let data = { ...dataheaderJson, registerId: contextrefOTP.registerId };
    const resp = await axiosPost("/register/resend-otp.service", data);
    console.log("Data : ", resp);
    console.log("Data1 : ", resp.res.responseCode);

    if (resp.data.responseCode === "200") {
      history.push("/registersuccess");
      return;
    }

    if (resp.res.responseCode !== "200") {
      setErrorM(
        resp.res.responseStatus +
          ":" +
          resp.res.responseCode +
          " " +
          resp.res.responseMessage
      );
      // setDisplayerrorTitle(resp.res.responseCode);
      // setDisplayerrorMessage(resp.res.responseMessage);
      // setDisplayerror("none");
      // setDisplayerror("block");
      // console.log("all 400 500", resp.res.responseMessage);
    } else {
      setErrorM(
        resp.res.responseStatus +
          ":" +
          resp.res.responseCode +
          " " +
          resp.res.responseMessage
      );
      // setDisplayerrorTitle(resp.res.responseCode);
      // setDisplayerrorMessage(resp.res.responseMessage);
      // setDisplayerror("none");
      // setDisplayerror("block");
      // setErrorM(resp.res.responseMessage);
      return;
    }
  };
  const handleChange = (e) => {
    const { target } = e;
    const { name } = target;
    const value = target.value;
    setInput({
      ...inputotp,
      [name]: value,
    });
  };
  return (
    <div id="root-white">
      <Errordialog
        open={open}
        title={displayerrorTitle}
        message={displayerrorMessage}
      />
      <div className="menu-header-blank">
        <div className="c-header-center">กรุณาลงทะเบียน</div>
      </div>
      <div className="content">
        <center>
          <h6>กรอกรหัส OTP</h6>
          <h6>
            รหัสได้ถูกส่งไปยัง <b>{contextrefOTP.mobileNo}</b>
          </h6>
        </center>
        <div>
          <Container>
            <Row>
              <Col>
                <Form noValidate validated={validated}>
                  <Row className="mb-4">
                    <Form.Group className="mb-4">
                      <Form.Label
                        style={{
                          float: "left",
                          color: "#757575",
                          marginTop: "1rem",
                        }}
                      >
                        รหัส OTP
                      </Form.Label>
                      <Form.Label
                        style={{
                          float: "right",
                          color: "#757575",
                          marginTop: "1rem",
                        }}
                      >
                        REF: {contextrefOTP.otpRef}
                      </Form.Label>
                      <Form.Control
                        required
                        type="number"
                        placeholder="000000"
                        name="otp"
                        id="otp"
                        // maxLength="50"
                        // max="6"
                        // step="2"
                        // style={{ borderColor: "#16A199" }}
                        onChange={handleChange}
                      />
                      <Form.Text className="text-danger"></Form.Text>
                    </Form.Group>
                  </Row>
                </Form>
                <Row>
                  <center>
                    <h6 style={{ color: "#757575" }}>
                      ยังไม่ได้รับรหัส OTP{" "}
                      <span
                        onClick={handleSendOTPAgain}
                        style={{ textDecoration: "none", color: "#757575" }}
                      >
                        ส่งอีกครั้ง (29)
                      </span>
                    </h6>
                    <Form.Text className="text-danger">
                      {errorMessage}
                    </Form.Text>
                  </center>
                </Row>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
      <div>
        <div className="menu-footer">
          <button
            className="btn-green"
            type="button"
            name="nextstep"
            onClick={handleSubmit}
            style={{ margin: "1rem" }}
          >
            ถัดไป
          </button>
        </div>
      </div>
    </div>
  );
};
export default ConfirmOTP;
