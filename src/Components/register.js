import React, { useState, useContext, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container, Form, Row, Col } from "react-bootstrap";
import Errordialog from "./errordialog";
import { axiosPost } from "../lib/withPost";
import { Redirect } from "react-router";
import dataheaderJson from "../Components/dataheaderAxios";
import { CounterContext } from "../store/CounterProvider";
import history from "./history";

const RegisterNew = (props) => {
  const { contextrefOTP, addREFOTP } = useContext(CounterContext);
  const [validated, setValidated] = useState(false);
  const [validatedPass, setValidatedPass] = useState("");
  const [validatedConfirmPass, setValidatedConfirmPass] = useState("");
  const [open, setDisplayerror] = useState("none");
  const [displayerrorTitle, setDisplayerrorTitle] = useState("");
  const [displayerrorMessage, setDisplayerrorMessage] = useState("");
  const [input, setInput] = useState({
    name: "",
    sname: "",
    email: "",
    mobileNo: "",
    password: "",
  });

  useEffect(() => {
    setDisplayerror("none");
  }, [props.open]);

  const styles = (theme) => ({
    content1: {
      overflowY: "scroll",
    },
    footern: {
      background: "#1b4e4a !importent",
      padding: "1rem !importent",
    },
  });

  const handleSubmit = async (event) => {
    const form = event.currentTarget;
    let isValid = false;

    if (input.password == "") {
      isValid = false;
    } else {
      setValidatedPass("");
    }
    if (
      input.password !== "undefined" &&
      input.confirm_password !== "undefined"
    ) {
      if (input.password != "" && input.confirm_password != "") {
        if (input.password != input.confirm_password) {
          isValid = false;
          setValidatedConfirmPass("Passwords don't match.");
        } else {
          setValidatedConfirmPass("");
          isValid = true;
        }
      } else {
        isValid = false;
        setValidatedPass("Please enter your password.");
        setValidatedConfirmPass("Passwords don't match.");
      }
    } else {
      isValid = false;
    }

    if (isValid === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    setValidated(true);

    if (isValid === true) {
      let data = { ...dataheaderJson, ...input };
     // console.log("data", data);
      const resp = await axiosPost("/register/register-request.service", data);
      //console.log("data", resp);
      if (resp.data.responseCode == "200") {
        addREFOTP({
          mobileNo: input.mobileNo,
          registerId: resp.data.registerId,
          otpRef: resp.data.otpRef,
        });
        history.push("/register-confirm-otp");
        return;
      } else {
        setDisplayerrorTitle(resp.data.responseCode);
        setDisplayerrorMessage(resp.data.responseMessage);
        setDisplayerror("none");
        setDisplayerror("block");

        // console.log("DD", open);
      }

      // if (resp.err.response.data.responseCode == "200") {
      //   console.log("error", resp);
      // } else {
      //   setDisplayerrorTitle(resp.data.responseCode);
      //   setDisplayerrorMessage(resp.data.responseMessage);
      //   setDisplayerror(true);
      // }

      //   .then((res) => {
      //     // console.log("data=" + res.data);
      //     if (res.data.responseCode == "200") {
      //       addREFOTP({
      //         mobileNo: input.mobileNo,
      //         registerId: res.data.registerId,
      //         otpRef: res.data.otpRef,
      //       });
      //       history.push("/register-confirm-otp");
      //     } else {
      //       setDisplayerrorTitle(res.data.responseCode);
      //       setDisplayerrorMessage(res.data.responseMessage);
      //       setDisplayerror("block");
      //     }
      //   })
      // .catch((error) => {
      //   // setDisplayerrorTitle(error.response.data.responseCode);
      //   // setDisplayerrorMessage(error.response.data.responseMessage);
      //   // setDisplayerror("block");
      //   console.log.apply("error:", error.response.data.responseMessage);
      //   //alert("error" + error.response.data.responseMessage);
      // });
    }
  };

  const handleChange = (e) => {
    const { target } = e;
    const { name } = target;
    const value = target.value;
    setInput({
      ...input,
      [name]: value,
    });
  };
  return (
    <div>
      <Errordialog
        open={open}
        title={displayerrorTitle}
        message={displayerrorMessage}
      />
      <div className="menu-header-blank">
        <div className="c-header-center">กรุณาลงทะเบียน</div>
      </div>
      <div className="content" style={styles.content1}>
        <center>
          <h6>กรอกข้อมูลส่วนตัว{open}</h6>
        </center>
        <div>
          <Container>
            <Row>
              <Col>
                <Form noValidate validated={validated}>
                  <Row className="mb-3">
                    <Form.Group className="mb-3">
                      <Form.Label>ชื่อ</Form.Label>
                      <Form.Control
                        required
                        type="text"
                        placeholder="ชื่อ"
                        defaultValue=""
                        name="name"
                        id="name"
                        maxLength="50"
                        onChange={handleChange}
                      />
                      <Form.Text className="text-danger"></Form.Text>
                    </Form.Group>
                    <Form.Group className="mb-3">
                      <Form.Label>นามสกุล</Form.Label>
                      <Form.Control
                        required
                        type="text"
                        placeholder="นามสกุล"
                        defaultValue=""
                        name="sname"
                        id="sname"
                        maxLength="50"
                        onChange={handleChange}
                      />
                      <Form.Text className="text-danger"></Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3">
                      <Form.Label>อีเมล์</Form.Label>
                      <Form.Control
                        required
                        type="email"
                        placeholder="example@email.com"
                        name="email"
                        id="email"
                        maxLength="50"
                        onChange={handleChange}
                      />
                      <Form.Text className="text-muted"></Form.Text>
                    </Form.Group>
                    <Form.Group as={Col} className="mb-3">
                      <Form.Label>เบอร์โทรศัพท์มือถือ</Form.Label>
                      <Form.Control
                        required
                        type="tel"
                        placeholder="088 004 0025"
                        defaultValue=""
                        name="mobileNo"
                        id="mobileNo"
                        maxLength="10"
                        onChange={handleChange}
                      />
                      <Form.Text className="text-danger"></Form.Text>
                    </Form.Group>
                    <Form.Group className="mb-3">
                      <Form.Label>รหัสผ่าน</Form.Label>
                      <Form.Control
                        required
                        type="password"
                        name="password"
                        onChange={handleChange}
                        placeholder="********"
                      />
                      <Form.Text className="text-danger">
                        {validatedPass}
                      </Form.Text>
                    </Form.Group>
                    <Form.Group className="mb-3">
                      <Form.Label>กรอกรหัสผ่านอีกครั้ง</Form.Label>
                      <Form.Control
                        required
                        type="password"
                        name="confirm_password"
                        onChange={handleChange}
                        placeholder="********"
                      />
                      <Form.Text className="text-danger">
                        {validatedConfirmPass}
                      </Form.Text>
                    </Form.Group>

                    <h1>{contextrefOTP.id}</h1>
                  </Row>
                </Form>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
      <div className="menu-footer">
        <button
          className="btn-green"
          type="submit"
          name="nextstep"
          onClick={handleSubmit}
          style={{ margin: "1rem" }}
        >
          ถัดไป
        </button>
      </div>
    </div>
  );
};
export default RegisterNew;
