import React, {  useState,useEffect } from "react";
const Errordialog  = (props) => {
  const [open, setOpen] = useState("none");
  const [data, setData] = useState({
    title : "Error",
    message : ""
  });
  useEffect(() => {
    setOpen(props.open);
    setData({
      title : props.title,
      message : props.message
    })
  }, [props.open]);

  const handleClose = (event) => {
    setOpen("none");
    setData({
      title : "",
      message : ""
    })
  };
    return (
      <div>
        <div className="dialog-error" style={{ display : open}}>
          <div className="dialog-error-block">
            <div className="dialog-error-title">{data.title}</div>
            <div className="dialog-error-message">{data.message}</div>
            <button
              className="btn-red"
              type="button"
              name="tel"
             onClick={handleClose}
            >
             ตกลง
            </button>
          </div>
        </div>
      </div>
    );
  }

export default Errordialog;
