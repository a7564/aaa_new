import "../index.css";
import React, { useContext} from "react";
import Img from "../asset/logo/logo.svg";
import ImgFrame2179 from "../asset/images/Frame2179.png";
import { useSelector } from "react-redux";
import { CounterContext } from "../store/CounterProvider"

const RegisterSuccess = (props) => {
    const {counter, addCounter ,subCounter} = useContext(CounterContext);
  const imgcenter = {
    margin: "15% 0 15% 0",
  };
  const white = {
    color: "#FFF",
  };
  //const { user } = useSelector((state) => ({ ...state }));
 
  
  //console.log("111" + user);
  return (
    <div>
      <div id="root-green">
        <div className="menu-header-blank">
          <div className="c-header-center">
            <center>
              {" "}
              <img src={Img} alt="logocenter"></img>
            </center>
          </div>
        </div>
        <div className="content">
          <br></br>
          <center>
            {" "}
            <img src={ImgFrame2179} alt="logocenter2" style={imgcenter}></img>
            <br></br>
            <h1 style={white}>สมัครใช้บริการสำเร็จ {counter}</h1>
          </center>
        </div>

        <div className="menu-footer">
          <button
            className="btn-green"
            type="button"
            name="nextstep"
            style={{ margin: "1rem" }}
          >
            ดำเนินการต่อไป
          </button>
        </div>
      </div>
    </div>
  );
};


export default RegisterSuccess;
