import Axios from "axios";
let config = {
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json; charset=utf-8",
    accept: "application/json",
  },
};
export const axiosGet = (path) => {
  return Axios.get(process.env.REACT_APP_API + path, config).then((res) => {
    return res.data;
  });
};
export const axiosPost = (path, dataR) => {
  let data = trimObj(dataR);
  // alert(process.env.REACT_APP_API + path + "="+data)
  return Axios.post(process.env.REACT_APP_API + path, data, config,{mode:'no-cors'})
    .then((res) => {
      return res;
    })
    .catch((error) => { 
      let res =  error.response.data
    return Promise.resolve({ res });
    });
};
export const axiosPut = (path, dataR) => {
  let data = trimObj(dataR);
  return Axios.put(process.env.REACT_APP_API + path, data, config).then(
    (res) => {
      window.loading.onLoaded();

      return res;
    }
  );
};
export const axiosdelete = (path, dataR, config) => {
  let data = trimObj(dataR);
  return Axios.delete(process.env.REACT_APP_API + path, data).then((res) => {
    window.loading.onLoaded();

    return res;
  });
};
export const axiosAll = (pathArray) => {
  return Axios.all(pathArray).then((res) => {
    return res;
  });
};

function trimObj(obj) {
  if (obj === null) {
    return null;
  }

  if (!Array.isArray(obj) && typeof obj != "object") return obj;
  return Object.keys(obj).reduce(
    function (acc, key) {
      acc[key.trim()] =
        typeof obj[key] == "string" ? obj[key].trim() : trimObj(obj[key]);
      return acc;
    },
    Array.isArray(obj) ? [] : {}
  );
}
