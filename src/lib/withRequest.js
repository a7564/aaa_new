import React, { Component } from "react";
import axios from "axios";
import HTTP from '../lib/withPost'

export default (WrappedComponent) =>
  class extends Component {
    state = {
        data = []
    };

      async componentDidMount(){
    
        const result = await HTTP.get(url);
        this.setState({data: result.data});
          
    }
    render() {
        return <WrappedComponent {...this.state}{...this.props}/>
    }
  };
